My nanorc as well as a collection of nano syntax highlighting files, mostly copied from <https://github.com/scopatz/nanorc>, but with some modification, most notably the C/C++ file has been split into separate files with the C++ highlighting working with C++17.

Running the install.sh script will overwrite your nanorc, so back it up beforehand.

All files are licensed under the GPLv3 and retain the copyright notice of scopatz as required by the GPL license. Any modified files will have my copyright notice on it as well. Any files I made entirely myself will be copyright to just me.

